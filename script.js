async function findIpAddress() {
  const containerInfo = document.querySelector('#container');

  const response = await fetch('https://api.ipify.org/?format=json');
  const data = await response.json();
  const clientIp = data.ip;

  const address = await fetch(`http://ip-api.com/json/${clientIp}?fields=continent,country,regionName,city,district`);
  const addressData = await address.json();

  Object.keys(addressData).forEach(key => {
    addressData[key] ? addressData[key] : addressData[key] = `Can't find ${key}`;
  });

  containerInfo.innerHTML = `
      <h4><span>Continent:</span> ${addressData.continent}</h4>
      <h4><span>Country:</span> ${addressData.country}</h4>
      <h4><span>Region:</span> ${addressData.regionName}</h4>
      <h4><span>City:</span> ${addressData.city}</h4>
      <h4><span>District:</span> ${addressData.district}</h4>
      `;
}   